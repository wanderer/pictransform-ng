package pictransform_ng;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.jhlabs.image.EmbossFilter;
import com.jhlabs.image.GaussianFilter;
import com.jhlabs.image.LensBlurFilter;
import com.jhlabs.image.OpacityFilter;

@WebServlet(asyncSupported = true, description = "pictransform-ng servlet", urlPatterns = { "/MainServlet" })
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private InputStream original_input_stream;
	private String original_content_type;
	private BufferedImage original;
	private BufferedImage transformed;
	private String[] transformations = {"Gaussian","Lens Blur"};

	private void base_top(PrintWriter out) {
		String outf = "<html lang=\"en\">"+"<head>" +
		"<title>pictransform servlet</title>" +
		"<meta charset=\"utf-8\" style=\"scroll-behavior: unset;\">" +
		"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" style=\"scroll-behavior: unset;\">" +
		"</head> <body>" +
		"<h1>pictransform-ng</h1>";
		out.println(outf);
	}

	private void base_bottom(PrintWriter out) {
		out.println("</body>"+"</html>");
	}
	private void base_filter_select(PrintWriter out, String uri, String sel_filter) {
		String l_gauss = uri + "?Gaussian";
		String l_lensblur = uri + "?LensBlur";
		String l_opacity = uri + "?Opacity";
		String l_emboss = uri + "?Emboss";
		String outf = "";

		outf += "<br /><span class=\"text\"><a href=\"" + l_gauss + "\" rel=\"noopener\">Gaussian</a></span>";
		outf += "<br /><span class=\"text\"><a href=\"" + l_lensblur + "\" rel=\"noopener\">LensBlur</a></span>";
		outf += "<br /><span class=\"text\"><a href=\"" + l_opacity + "\" rel=\"noopener\">Opacity</a></span>";
		outf += "<br /><span class=\"text\"><a href=\"" + l_emboss + "\" rel=\"noopener\">Emboss</a></span>";
		out.println(outf);
	}

	private void gaussian_form(PrintWriter out, int selRadius, String path) {
		int radia[] = {1, 5, 10, 50};
		String outf = "<h2>FourierTransform at " + path + "</h2>" +
		"<form  method='post' enctype='multipart/form-data'>\n" +
		"<input name='image' type='file'/>" +
		"Radius:" +
		"<select name='radius'>\n";
		for (int r : radia) {
			outf += "<option value='" + r + "'";
			if (r == selRadius) {
				outf += "selected='selected'";
			}
			outf += ">" + r + "</option>";
		}
		outf += "</select>\n" +
		"<input type='submit' value='Send'>\n" +
		"<form>";
		out.println(outf);
	}

	private void lensblur_form(PrintWriter out, float selRadius, int sides, float bloom, float bloom_threshold, String path) {
		String title = "LensBlur";
		int radia[] = {1, 5, 10, 50};
		int f_sides[] = {1, 2, 3, 4, 5, 10};
		int f_bloom[] = {1, 2, 3, 4, 5, 10};
		int f_bloom_thr[] = {1, 2, 3, 4, 5, 10};

		String outf = "<h2>" + title + " at " + path + "</h2>";
		outf += "<form  method='post' enctype='multipart/form-data'>\n" +
		"<input name='image' type='file'/>" +
		"Radius: " +
		"<select name='radius'>\n";
		for (float r : radia) {
			outf += "<option value='" + r + "'";
			if (r == selRadius) {
				outf += "selected='selected'";
			}
			outf += ">" + r + "</option>";
		}
		outf += "</select>\n";
		outf += "Sides: " +
		"<select name='sides'>\n";
		for (int s : f_sides) {
			outf += "<option value='" + s + "'";
			if (s == sides) {
				outf += "selected='selected'";
			}
			outf += ">" + s + "</option>";
		}
		outf += "</select>\n";
		outf += "Bloom: " +
		"<select name='bloom'>\n";
		for (float b : f_bloom) {
			outf += "<option value='" + b + "'";
			if (b == bloom) {
				outf += "selected='selected'";
			}
			outf += ">" + b + "</option>";
		}
		outf += "</select>\n";
		outf += "Bloom threshold: " +
		"<select name='bloom_thr'>\n";
		for (float bt : f_bloom_thr) {
		outf += "<option value='" + bt + "'";
			if (bt == bloom_threshold) {
				outf += "selected='selected'";
			}
			outf += ">" + bt + "</option>";
		}
		outf += "</select>\n" +
		"<input type='submit' value='Send'>\n" +
		"<form>";
		out.println(outf);
	}

	private void opacity_form(PrintWriter out, int opacity, String path) {
		int opacities[] = {1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
		String outf = "<h2>Opacity at " + path + "</h2>" +
		"<form  method='post' enctype='multipart/form-data'>\n" +
		"<input name='image' type='file'/>" +
		"Opacity:" +
		"<select name='opacity'>\n";
		for (int o : opacities) {
			outf += "<option value='" + o + "'";
			if (o == opacity) {
				outf += "selected='selected'";
			}
			outf += ">" + o + "</option>";
		}
		outf += "</select>\n" +
		"<input type='submit' value='Send'>\n" +
		"<form>";
		out.println(outf);
	}

	private void emboss_form(PrintWriter out, float azimuth, float elevation, float bump_height, String path) {
		float azimuth_f[] = {0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330};
		float elevation_f[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		float bump_height_f[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9 , 10};
		String outf = "<h2>Emboss at " + path + "</h2>" +
		"<form  method='post' enctype='multipart/form-data'>\n" +
		"<input name='image' type='file'/>" +
		"Azimuth:" +
		"<select name='opacity'>\n";
		for (float a : azimuth_f) {
			outf += "<option value='" + a + "'";
			if (a == azimuth) {
				outf += "selected='selected'";
			}
			outf += ">" + a + "</option>";
		}
		outf += "</select>\n" +
		"Elevation:" +
		"<select name='opacity'>\n";
		for (float e : elevation_f) {
			outf += "<option value='" + e + "'";
			if (e == elevation) {
				outf += "selected='selected'";
			}
			outf += ">" + e + "</option>";
		}
		outf += "</select>\n" +
		"Bump Height:" +
		"<select name='opacity'>\n";
		for (float b : bump_height_f) {
			outf += "<option value='" + b + "'";
			if (b == bump_height) {
				outf += "selected='selected'";
			}
			outf += ">" + b + "</option>";
		}
		outf += "</select>\n" +
		"<input type='submit' value='Send'>\n" +
		"<form>";
		out.println(outf);
	}

	protected String process_not_jpeg(String orig_ctype){
		String outf = "<div style=\"background-color:red; margin: 2\">Cannot use filters on any other than JPEG files.</div>"+
		"<div style=\"background-color:red; margin: 2\">Content type of the image: " + orig_ctype + "</div>";
		return outf;
	}

	protected void process_gaussian(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException {
		//response.setContentType("text/html;charset=utf-8");
		float selRadius = 1;
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileupload = new ServletFileUpload(factory);
		List<FileItem> items = fileupload.parseRequest(request);

		for (FileItem i : items) {
			if (i.isFormField()) {
				String fieldName = i.getFieldName();
				String fieldValue = i.getString();
				if (fieldName.equals("radius")) {
					selRadius = Float.parseFloat(fieldValue);
				}
			} else { // fileupload
				String filename=i.getName();
				long size=i.getSize();
				if (!filename.isEmpty() && size>0) {
					BufferedInputStream new_input_stream = new BufferedInputStream(i.getInputStream());
					new_input_stream.mark((int)i.getSize()+1);
					BufferedImage new_original = ImageIO.read(new_input_stream);
					if (new_original != null) {
						if (original_input_stream!=null)
							original_input_stream.close();
						original_input_stream=new_input_stream;
						original_content_type = i.getContentType();
						if (! original_content_type.matches("image/jpeg")) {
							process_not_jpeg(original_content_type);
							new_original = null;
							original = null;
						}
						original=new_original;
						new_original = null;
					}

				}
			}
		}
		if (original != null) {
			GaussianFilter gf = new GaussianFilter(selRadius);
			transformed = gf.filter(original, null);
		}
	}

	protected void process_lensblur(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException {
		//response.setContentType("text/html;charset=utf-8");

		float radius = 1;
		int sides = 1;
		float bloom = 1;
		float bloom_threshold = 1;

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileupload = new ServletFileUpload(factory);
		List<FileItem> items = fileupload.parseRequest(request);
		for (FileItem i : items) {
			if (i.isFormField()) {
				String fieldName = i.getFieldName();
				String fieldValue = i.getString();
				switch (fieldName) {
					case "radius":
						radius = Float.parseFloat(fieldValue);
						break;
					case "sides":
						sides = Integer.parseInt(fieldValue);
						break;
					case "bloom":
						bloom = Float.parseFloat(fieldValue);
						break;
					case "bloom_thr":
						bloom_threshold = Float.parseFloat(fieldValue);
						break;
					default:
						break;
				}
			} else { // fileupload
				String filename=i.getName();
				long size=i.getSize();
				if (!filename.isEmpty() && size>0) {
					BufferedInputStream new_input_stream = new BufferedInputStream(i.getInputStream());
					new_input_stream.mark((int)i.getSize()+1);
					BufferedImage new_original = ImageIO.read(new_input_stream);
					if (new_original != null) {
						if (original_input_stream!=null)
							original_input_stream.close();
						original_input_stream=new_input_stream;
						original_content_type = i.getContentType();
						if (! original_content_type.matches("image/jpeg")) {
							process_not_jpeg(original_content_type);
							new_original = null;
							original = null;
						}
						original=new_original;
						new_original = null;
					}
				}
			}
		}
		if (original != null) {
			LensBlurFilter lbf = new LensBlurFilter();
			lbf.setRadius(radius);
			lbf.setSides(sides);
			lbf.setBloom(bloom);
			lbf.setBloomThreshold(bloom_threshold);
			transformed = lbf.filter(original, null);
			lbf.filter(original, transformed);
		}
	}

	protected void process_opacity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException {
		int opacity = 1;

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileupload = new ServletFileUpload(factory);
		List<FileItem> items = fileupload.parseRequest(request);

		for (FileItem i : items) {
			if (i.isFormField()) {
				String fieldName = i.getFieldName();
				String fieldValue = i.getString();
				switch (fieldName) {
					case "opacity":
						opacity = Integer.parseInt(fieldValue);
						break;
				}
			} else { // fileupload
				String filename=i.getName();
				long size=i.getSize();
				if (!filename.isEmpty() && size>0) {
					BufferedInputStream new_input_stream = new BufferedInputStream(i.getInputStream());
					new_input_stream.mark((int)i.getSize()+1);
					BufferedImage new_original = ImageIO.read(new_input_stream);
					if (new_original != null) {
						if (original_input_stream!=null)
							original_input_stream.close();
						original_input_stream=new_input_stream;
						original_content_type = i.getContentType();
						if (! original_content_type.matches("image/jpeg")) {
							process_not_jpeg(original_content_type);
							new_original = null;
							original = null;
						}
						original=new_original;
						new_original = null;
					}
				}
			}
		}
		if (original != null) {
			OpacityFilter of = new OpacityFilter();
			of.setOpacity(opacity);
			transformed = of.filter(original, null);
		}
	}

	protected void process_emboss(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException {
		float azimuth = 1;
		float elevation = 1;
		float bump_height = 1;

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fileupload = new ServletFileUpload(factory);
		List<FileItem> items = fileupload.parseRequest(request);

		for (FileItem i : items) {
			if (i.isFormField()) {
				String fieldName = i.getFieldName();
				String fieldValue = i.getString();
				switch (fieldName) {
					case "azimuth":
						azimuth = Float.parseFloat(fieldValue);
						break;
					case "elevation":
						elevation = Float.parseFloat(fieldValue);
						break;
					case "bump_height":
						bump_height = Float.parseFloat(fieldValue);
						break;
				}
			} else { // fileupload
				String filename=i.getName();
				long size=i.getSize();
				if (!filename.isEmpty() && size>0) {
					BufferedInputStream new_input_stream = new BufferedInputStream(i.getInputStream());
					new_input_stream.mark((int)i.getSize()+1);
					BufferedImage new_original = ImageIO.read(new_input_stream);
					if (new_original != null) {
						if (original_input_stream!=null)
							original_input_stream.close();
						original_input_stream=new_input_stream;
						original_content_type = i.getContentType();
						if (! original_content_type.matches("image/jpeg")) {
							process_not_jpeg(original_content_type);
							new_original = null;
							original = null;
						}
						original=new_original;
						new_original = null;
					}
				}
			}
		}
		if (original != null) {
			EmbossFilter ef = new EmbossFilter();
			ef.setEmboss(true);
			ef.setAzimuth(azimuth);
			ef.setElevation(elevation);
			ef.setBumpHeight(bump_height);
			transformed = ef.filter(original, null);
		}
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		int radius = 1;
		int opacity = 50;

		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				switch (request.getQueryString()) {
					case "Gaussian":
						process_gaussian(request, response);
						break;
					case "LensBlur":
						process_lensblur(request, response);
						break;
					case "Opacity":
						process_opacity(request, response);
						break;
					case "Emboss":
						process_emboss(request, response);
						break;
					default:
						break;
				}

			} catch (FileUploadException ex) {
				System.out.println(ex);
			}
		}

		try {
			base_top(out);
			String path = request.getContextPath();
			String servlet = request.getRequestURI();
			String sel_filter = request.getQueryString();
			base_filter_select(out, servlet, sel_filter);

			switch (sel_filter) {
				case "Gaussian":
					gaussian_form(out, radius, path);
					break;

				case "LensBlur":
					lensblur_form(out, radius, 1, 1, 1, path);
					break;

				case "Opacity":
					opacity_form(out, opacity, path);
					break;

				case "Emboss":
					emboss_form(out, 1, 1, 1, path);
					break;

				default:
					break;
			}

			if (original != null) {
				out.println("<div style=\"max-width:1200px;align:center;margin:6px 0;\"><img style=\"max-width:90%;height:auto;display:auto;margin-right=auto;margin-left=auto;padding:10px;\" src='" + servlet + "?getimage=original'</div></br>");
			}
			if (transformed != null) {
				out.println("<div style=\"max-width:1200px;align:center;margin:6px 0;\"><img style=\"max-width:90%;height:auto;display:auto;margin-right=auto;margin-left=auto;padding:10px;\" src='" + servlet + "?getimage=transformed'</div></br>");
			}
			base_bottom(out);
		} finally {
			out.flush();
			out.close();
		}
	}

	public MainServlet() {
	}

	/**
	 * @see Servlet#getServletConfig()
	 */
	public ServletConfig getServletConfig() {
		return null;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param_getimage = request.getParameter("getimage");
		if (original_input_stream!=null && param_getimage != null) {
			OutputStream out = response.getOutputStream();
			if (param_getimage.equals("original")) {
				original_input_stream.reset();
				response.setContentType(original_content_type);
				IOUtils.copy(original_input_stream,out);
			} else if (param_getimage.equals("transformed")) {
				response.setContentType("image/jpeg");
				ImageIO.write(transformed, "jpeg", out);
			}
			out.flush();
			out.close();
		} else {
			processRequest(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doHead(HttpServletRequest, HttpServletResponse)
	 */
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
